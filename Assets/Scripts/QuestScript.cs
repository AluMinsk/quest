﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestScript : MonoBehaviour
{
    [SerializeField] string gameTitle;
    //[SerializeField] string gameStoryText;
    [SerializeField] TextMeshProUGUI titleComponent;
    //[SerializeField] Text storyTextComponent;
    [SerializeField] TextMeshProUGUI storyTextComponent;
    [SerializeField] State startState;
    //[SerializeField] string gameLocationText;
    //[SerializeField] Text locationTextComponent;
    [SerializeField] TextMeshProUGUI locationTextComponent;


    State currentState;
    // Start is called before the first frame update
    void Start()
    {
        currentState = startState;
        titleComponent.text = gameTitle;
        //print ("Start");

        //string[] days = { "пн", "вт", "ср", "чт", "пт", "сб", "вс" };
        //for (int i = 0; i <= 6 ; i++)

        //{
        //    string aDay = days[i];
        //    print(aDay);
        //}
        
        //print("Stop");

    }

    // Update is called once per frame
    void Update()
    {

        storyTextComponent.text = currentState.GetStoryText();
        locationTextComponent.text = currentState.GetLocationText();

        for (int i = 0; i < currentState.GetNextStates().Length; i++)
        {
            if (Input.GetKeyDown(KeyCode.Keypad1 + i) || Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                State[] nextStates = currentState.GetNextStates();
                currentState = nextStates[i];
            }

        }
        //if (Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    State[] nextStates = currentState.GetNextStates();
        //    currentState = nextStates[0];
        //}
        //else if (Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    currentState = currentState.GetNextStates()[1];
        //}
        //else if (Input.GetKeyDown(KeyCode.Keypad3) || Input.GetKeyDown(KeyCode.Alpha3))
        //{
        //    currentState = currentState.GetNextStates()[2];
        //}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentState = startState;
        }
    }
}
