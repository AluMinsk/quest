﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
    [TextArea(9, 14)][SerializeField] string storyText;
    [TextArea(9, 5)] [SerializeField] string LocationText;
    [SerializeField] State[] nextStates;

    public string GetStoryText()
    {

        return storyText;
        
    }
    public string GetLocationText()
    {
        
        return LocationText;
    }

    public State[] GetNextStates()
    {
        return nextStates;

    }
}
